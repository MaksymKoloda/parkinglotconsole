﻿using System;
using System.Collections.Generic;
using Parking.Interfaces;
using Parking.Services;

namespace Parking.Entities
{
    public class ParkingLot
    {
        private readonly IVehicleService _vehicleService;
        private readonly ITransactionService _transactionService;

        private static readonly Lazy<ParkingLot> Lazy =
            new Lazy<ParkingLot>(() =>
                new ParkingLot(new TransactionService(new TransactionsLogService()), new VehicleService()));

        public static ParkingLot Instance => Lazy.Value;
        public int VehiclesCount => _vehicleService.Count;

        private ParkingLot(ITransactionService transactionService, IVehicleService vehicleService)
        {
            _transactionService = transactionService;
            _vehicleService = vehicleService;
        }

        public bool AddVehicle(Vehicle vehicle)
        {
            var added = _vehicleService.AddVehicle(vehicle);
            if (added)
            {
                _transactionService.StartTransactions(vehicle,
                    TimeSpan.FromSeconds(Configuration.TransactionInterval).TotalMilliseconds);
            }

            return added;
        }

        public void RemoveVehicle(int index)
        {
            var vehicleId = _vehicleService.GetVehicleId(index);
            
            _vehicleService.RemoveVehicle(vehicleId);

            _transactionService.EndTransactions(vehicleId);
        }

        public double GetBalance(TimeSpan timeSpan = default)
        {
            return _transactionService.GetBalance(timeSpan);
        }

        public IEnumerable<Transaction> GetLastTransactions()
        {
            return _transactionService.GetTransactions(TimeSpan.FromMinutes(1));
        }

        public IEnumerable<string> GetTransactionLogs()
        {
            return _transactionService.GetTransactionLogs();
        }

        public IEnumerable<Vehicle> GetVehicles()
        {
            return _vehicleService.GetVehicles();
        }

        public void AddToVehicleBalance(int index, int amount)
        {
            var vehicleId = _vehicleService.GetVehicleId(index);

            var vehicle = _vehicleService.GetVehicle(vehicleId);

            vehicle.AddToBalance(amount);
        }
    }
}