﻿using System;

namespace Parking.Entities
{
    public class Vehicle
    {
        private double _balance;

        public Guid Id { get; }

        public VehicleType Type { get; }

        public Vehicle(VehicleType type, double balance)
        {
            _balance = balance;
            Type = type;
            Id = Guid.NewGuid();
        }

        public bool CanPay(double bill)
        {
            return (_balance - bill >= 0);
        }

        public void Charge(double bill)
        {
            _balance -= bill;
        }

        public void AddToBalance(int value)
        {
            if (value >= 0)
            {
                _balance += value;
            }
        }

        public override string ToString()
        {
            return $"Id:{Id}; Type:{Type}; Balance:{_balance}";
        }
    }
}