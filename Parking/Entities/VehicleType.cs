﻿namespace Parking.Entities
{
    public enum VehicleType
    {
        Car,
        Truck,
        Bus,
        Motorcycle
    }
}