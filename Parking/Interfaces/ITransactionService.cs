﻿using System;
using System.Collections.Generic;
using Parking.Entities;

namespace Parking.Interfaces
{
    public interface ITransactionService
    {
        double GetBalance(TimeSpan timeSpan = default);

        void StartTransactions(Vehicle vehicle, double interval);

        void EndTransactions(Guid vehicleId);

        IEnumerable<Transaction> GetTransactions(TimeSpan timeSpan);

        IEnumerable<string> GetTransactionLogs();
    }
}