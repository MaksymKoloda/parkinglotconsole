﻿using System;
using System.Collections.Generic;
using Parking.Entities;

namespace Parking.Interfaces
{
    public interface IVehicleService
    {
        int Count { get; }

        bool AddVehicle(Vehicle vehicle);

        void RemoveVehicle(Guid vehicleId);

        Vehicle GetVehicle(Guid vehicleId);

        IEnumerable<Vehicle> GetVehicles(Func<Vehicle, bool> filter = default);

        Guid GetVehicleId(int index);
    }
}