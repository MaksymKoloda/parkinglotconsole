﻿using System;
using System.Collections.Generic;
using System.Linq;
using Parking.Entities;
using Parking.Interfaces;

namespace Parking.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly List<Vehicle> _vehicles = new List<Vehicle>();

        public int Count => _vehicles.Count;

        public Vehicle GetVehicle(Guid vehicleId)
        {
            return _vehicles.FirstOrDefault(v => v.Id == vehicleId);
        }

        public IEnumerable<Vehicle> GetVehicles(Func<Vehicle,bool> filter = default)
        {
            return filter == default ? _vehicles : _vehicles.Where(filter);
        }

        public Guid GetVehicleId(int index)
        {
            return _vehicles.ElementAt(index).Id;
        }

        public bool AddVehicle(Vehicle vehicle)
        {
            if (_vehicles.Count >= Configuration.VehicleCapacity)
            {
                return false;
            }

            _vehicles.Add(vehicle);

            return true;
        }

        public void RemoveVehicle(Guid vehicleId)
        {
            var vehicle = _vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle != null)
            {
                _vehicles.Remove(vehicle);
            }
        }
    }
}