﻿using System.Collections.Generic;
using System.IO;
using Parking.Interfaces;

namespace Parking.Services
{
    public class TransactionsLogService : ILogService
    {
        private const string Path = "Transactions.log";

        public void Log(IEnumerable<string> logs)
        {
            File.AppendAllLines(Path, logs);
        }

        public IEnumerable<string> GetLogs()
        {
            return File.ReadAllLines(Path);
        }
    }
}