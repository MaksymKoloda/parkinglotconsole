﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Parking.Entities;
using Parking.Interfaces;

namespace Parking.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ILogService _logService;

        private double _balance;

        private readonly ConcurrentDictionary<Guid, Transaction> _transactions = new ConcurrentDictionary<Guid, Transaction>();

        public TransactionService(ILogService logService)
        {
            _logService = logService;
            _balance = Configuration.InitialBalance;

            TimerService.AddTask(Guid.NewGuid(), () =>
            {
                RemoveOutDatedTransactions();
                logService.Log(_transactions.Values.OrderBy(t => t.Time).Select(t => t.ToString()));
            }, TimeSpan.FromMinutes(1).TotalMilliseconds);
        }

        private void RemoveOutDatedTransactions()
        {
            var keys = _transactions
                .Where(t => DateTime.UtcNow >= t.Value.Time.AddMinutes(1))
                .Select(t => t.Key);

            foreach (var key in keys)
            {
                _transactions.TryRemove(key, out _);
            }
        }

        private void CreateTransaction(Vehicle vehicle)
        {
            var bill = Configuration.Rates[vehicle.Type];
            var canPay = vehicle.CanPay(bill);
            if (!canPay)
            {
                bill *= Configuration.FineMultiplier;
            }

            vehicle.Charge(bill);
            _balance += bill;

            var transaction = new Transaction(vehicle.Id, bill);
            _transactions[transaction.Id] = transaction;
        }

        public void StartTransactions(Vehicle vehicle, double interval)
        {
            TimerService.AddTask(vehicle.Id, () => CreateTransaction(vehicle), interval);
        }

        public void EndTransactions(Guid vehicleId)
        {
            TimerService.EndTask(vehicleId);
        }

        public double GetBalance(TimeSpan timeSpan = default)
        {
            var balance = timeSpan == default
                ? _balance
                : _transactions.Values.Where(t => DateTime.UtcNow < t.Time.Add(timeSpan)).Sum(t => t.Bill);
            
            return balance;
        }

        public IEnumerable<Transaction> GetTransactions(TimeSpan timeSpan)
        {
            return _transactions.Values.Where(t => DateTime.UtcNow < t.Time.Add(timeSpan)).OrderBy(t => t.Time);
        }

        public IEnumerable<string> GetTransactionLogs()
        {
            return _logService.GetLogs();
        }
    }
}