﻿using System;
using System.IO;
using System.Linq;
using Parking.Entities;

namespace Parking.Client
{
    public static class Program
    {
        private static readonly ParkingLot ParkingLot = ParkingLot.Instance;

        public static void Main()
        {
            Init();
            
            Run();
        }

        private static void Init()
        {
            File.Delete("Transactions.log");

            ParkingLot.AddVehicle(new Vehicle(VehicleType.Car, 20));
            ParkingLot.AddVehicle(new Vehicle(VehicleType.Bus, 50));
            ParkingLot.AddVehicle(new Vehicle(VehicleType.Truck, 80));
            ParkingLot.AddVehicle(new Vehicle(VehicleType.Motorcycle, 10));
        }

        private static void Run()
        {
            var choice = 0;
            while (choice != 10)
            {
                choice = Menu.Display();

                switch (choice)
                {
                    case 1:
                        ShowBalance();
                        break;
                    case 2:
                        ShowIncome();
                        break;
                    case 3:
                        ShowStatus();
                        break;
                    case 4:
                        ShowLastTransactions();
                        break;
                    case 5:
                        ShowTransactionLogs();
                        break;
                    case 6:
                        ShowVehicles();
                        break;
                    case 7:
                        CreateVehicle();
                        break;
                    case 8:
                        RemoveVehicle();
                        break;
                    case 9:
                        RefillVehicle();
                        break;
                    case 10:
                        return;
                    default:
                        continue;
                }
            }
        }

        private static void RefillVehicle()
        {
            try
            {
                Console.Write("Enter vehicle spot: ");
                var input = Console.ReadLine();
                if (int.TryParse(input, out var index))
                {
                    Console.Write("Enter amount: ");
                    input = Console.ReadLine();
                    if (int.TryParse(input, out var amount))
                    {
                        ParkingLot.AddToVehicleBalance(index, amount);
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void RemoveVehicle()
        {
            try
            {
                Console.Write("Enter vehicle spot: ");
                var input = Console.ReadLine();
                if (int.TryParse(input, out var index))
                {
                    ParkingLot.RemoveVehicle(index);
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void CreateVehicle()
        {
            var i = 0;
            foreach (var type in Enum.GetValues(typeof(VehicleType)))
            {
                Console.WriteLine($"{i} - {type}");
                i++;
            }

            Console.Write("Enter vehicle type: ");
            var input = Console.ReadLine();
            if (Enum.TryParse<VehicleType>(input, out var vehicleType))
            {
                Console.Write("Enter vehicle starting balance: ");
                input = Console.ReadLine();
                if (int.TryParse(input, out var balance))
                {
                    var vehicle = new Vehicle(vehicleType, balance);
                    ParkingLot.AddVehicle(vehicle);
                }
            }
        }

        private static void ShowVehicles()
        {
            Console.WriteLine("Parked vehicles:");

            var vehicles = ParkingLot.GetVehicles();
            if (!vehicles.Any())
            {
                Console.WriteLine("There are no vehicles.");
            }
            else
            {
                var i = 0;
                foreach (var vehicle in vehicles)
                {
                    Console.WriteLine($"{i,2} - {vehicle}");
                    i++;
                }
            }
        }

        private static void ShowTransactionLogs()
        {
            try
            {
                Console.WriteLine("Transaction logs:");
                foreach (var log in ParkingLot.GetTransactionLogs())
                {
                    Console.WriteLine(log);
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void ShowLastTransactions()
        {
            Console.WriteLine("Transactions for the last minute:");
            foreach (var transaction in ParkingLot.GetLastTransactions())
            {
                Console.WriteLine(transaction);
            }
        }

        private static void ShowStatus()
        {
            var taken = ParkingLot.VehiclesCount;
            var free = Configuration.VehicleCapacity - taken;
            Console.WriteLine($"Occupied spots:{taken}, free spots:{free}");
        }

        private static void ShowIncome()
        {
            var income = ParkingLot.GetBalance(TimeSpan.FromMinutes(1));
            Console.WriteLine($"Income for the last minute:{income}");
        }

        private static void ShowBalance()
        {
            var balance = ParkingLot.GetBalance();
            Console.WriteLine($"Balance:{balance}");
        }
    }
}